'use strict';

import React from 'react';
import { connect } from 'react-redux';
import PurchaseOptions from './components/purchaseOptions';
import Stock from './components/stock';
import Print from './components/print';

class Container extends React.Component {
	constructor(props) {
		super(props);
	}

	getCanvas(id, component) {
		return id === this.props.currentCanvas || null === this.props.currentCanvas && id === this.props.previousCanvas
			? component : null;
	}

	render() {
		const styles = {
			canvas: {
				backgroundImage: 'url(' + this.props.image + ')'
			}
		};

		const stock = this.getCanvas('buyStock', (<Stock />));
		const print = this.getCanvas('buyPrint', (<Print />));
		const options = this.getCanvas('options', (<PurchaseOptions />));


		const buyButton = 'options' !== this.props.currentCanvas
			? <button className="btn-flat primary" onClick={this.props.buyOptionsListener}>BUY</button>
			: null;

		const cancelButton = null !== this.props.currentCanvas
			? <button className="btn-flat" onClick={this.props.cancelListener}>CANCEL</button>
			: null;

		return (
			<figure className="container">
				<div className="canvas" style={styles.canvas}>
					{stock}
					{print}
					{options}
				</div>
				<div className="supplemental">
					<figcaption className="supplemental__caption">{this.props.caption}</figcaption>
					{cancelButton}
					{buyButton}
				</div>
			</figure>
		);
	}
};

const mapStateToProps = (store) => {
	return {
		currentCanvas: store.canvas.current,
		previousCanvas: store.canvas.previous
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		cancelListener: () => dispatch({type: 'CLEAR_CANVAS'}),
		buyOptionsListener: () => dispatch({type: 'UPDATE_CANVAS', id: 'options'})
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Container);