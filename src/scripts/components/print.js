'use strict';

import React from 'react';
import { connect } from 'react-redux';

class Print extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		let modifier = this.props.visible ? 'visible' : 'invisible';

		return (
			<section className="canvas__print">
				<form method="post" action="" className={'product product--' + modifier}>
					<legend className="product__label">PRINT</legend>
					<div>
						<label className="product__option">
							<input type="radio" name="product_option" value="standard" defaultChecked className="product__optionindicator product__optionindication--checked" />
							<span className="product__optionlabel">Standard License</span>
							<span className="product__optionprice">£0.79</span>
						</label>

						<label className="product__option">
							<input type="radio" name="product_option" value="extended" className="product__optionindicator" />
							<span className="product__optionlabel">Extended License</span>
							<span className="product__optionprice">£4.99</span>
						</label>
					</div>
				</form>
				<a href="" className={'product__altlabel product__altlabel--' + modifier} onClick={this.props.buyStockListener}>STOCK</a>
			</section>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		visible: null !== store.canvas.current
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		// buyStockListener: () => dispatch({type: 'PRINT_OPTION_REGULAR'}),
		// buyPrintListener: () => dispatch({type: 'PRINT_OPTION_LARGE'}),
		buyStockListener: function (event) {
			event.preventDefault();
			dispatch({type: 'UPDATE_CANVAS', id: 'buyStock'});
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Print);