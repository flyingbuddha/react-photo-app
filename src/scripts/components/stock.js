'use strict';

import React from 'react';
import { connect } from 'react-redux';

class Stock extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		let modifier = this.props.visible ? 'visible' : 'invisible';

		return (
			<section className="canvas__stock">
				<a href="" className={'product__altlabel product__altlabel--' + modifier} onClick={this.props.buyPrintListener}>PRINT</a>
				<form method="post" action="" className={'product product--' + modifier}>
					<legend className="product__label">STOCK</legend>
					<div>
						<label className="product__option">
							<input type="radio" name="product_option" value="standard" defaultChecked className="product__optionindicator product__optionindication--checked" />
							<span className="product__optionlabel">Standard License</span>
							<span className="product__optionprice">£0.79</span>
						</label>

						<label className="product__option">
							<input type="radio" name="product_option" value="extended" className="product__optionindicator" />
							<span className="product__optionlabel">Extended License</span>
							<span className="product__optionprice">£4.99</span>
						</label>
					</div>
				</form>
			</section>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		visible: null !== store.canvas.current
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		// buyStockListener: () => dispatch({type: 'STOCK_OPTION_STANDARD'}),
		// buyPrintListener: () => dispatch({type: 'STOCK_OPTION_EXTENDED'}),
		buyPrintListener: function (event) {
			event.preventDefault();
			dispatch({type: 'UPDATE_CANVAS', id: 'buyPrint'});
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Stock);