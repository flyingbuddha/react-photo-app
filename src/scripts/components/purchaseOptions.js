'use strict';

import React from 'react';
import { connect } from 'react-redux';

class PurchaseOptions extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {

		let modifier = this.props.visible ? 'visible' : 'invisible';

		return (
			<ul className="canvas__options">
				<li className={'canvas__option canvas__printoption--' + modifier + ' slide'}>
					<a href="" className="canvas__optionlink" onClick={this.props.buyPrintListener}>PRINT</a>
				</li>
				<li className={'canvas__option canvas__stockoption--' + modifier + ' slide'}>
					<a href="" className="canvas__optionlink" onClick={this.props.buyStockListener}>STOCK</a>
				</li>
			</ul>
		);
	}
}

const mapStateToProps = (store) => {
	return {
		visible: null !== store.canvas.current
	}
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		buyStockListener: function (event) {
			event.preventDefault();
			dispatch({type: 'UPDATE_CANVAS', id: 'buyStock'});
		},
		buyPrintListener: function (event) {
			event.preventDefault();
			dispatch({type: 'UPDATE_CANVAS', id: 'buyPrint'});
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseOptions);