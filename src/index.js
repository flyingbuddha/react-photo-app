'use strict';

import 'styles/base';

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import Container from 'scripts/container';

import {createStore} from 'redux';

const appReducer = (state = {}, action) => {

	console.log('Reducer called', state, action);

	let newState = {
		canvas: {
			current: null,
			previous: null
		}
	};

	switch(action.type) {
		case 'UPDATE_CANVAS':
			Object.assign(newState, state, {
				canvas: {
					current: action.id,
					previous: state.canvas.current || null
				}
			});
			break;

		case 'CLEAR_CANVAS':
			Object.assign(newState, state, {
				canvas: {
					current: null,
					previous: state.canvas.current || null
				}
			});
			break;

		default:
			Object.assign(newState, state);
			break;
	}

	console.info('New state', newState);

	return newState;
}

for (let element of document.querySelectorAll('.react-photo-app')) {
	(function (element, store) {
		ReactDOM.render(
			<Provider store={store}>
				<Container
					image={element.getAttribute('data-image')}
					caption={element.getAttribute('data-caption')}
				/>
			</Provider>,
			element
		);

 	}) (element, createStore(appReducer))
}